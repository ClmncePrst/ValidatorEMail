console.log("Hello, World!");
const validator = require('validator');
const emailsArray = ['clemence.prousteau@gmail.com', 'invalidmail', 'mail@example.com', 'isthismail@mail.com'];
function validateEmail(emailsArray) {
    return validator.isEmail(emailsArray);
}
emailsArray.forEach((emailsArray) => {
    console.log('Adresse email: ' + emailsArray);
    console.log('Valide: ' + validateEmail(emailsArray));
});